package org.nocoder.requirement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author YANGJINLONG
 */
@SpringBootApplication
public class RequirementApplication {

	public static void main(String[] args) {
		SpringApplication.run(RequirementApplication.class, args);
	}
}
