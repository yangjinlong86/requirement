package org.nocoder.requirement.model;

import java.io.Serializable;
import java.util.Date;

/**
 * @author YANGJINLONG
 */
public class Requirement implements Serializable {
    private Integer id;

    private String modelId;

    private String priority;

    private String reqSource;

    private String reqApplicant;

    private String reqCollector;

    private Date researchDate;

    private Date confirmDate;

    private String estimate;

    private String planResolutionVersion;

    private String realResolutionVersion;

    private String comment;

    private String originalRequirement;

    private String userStory;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getModelId() {
        return modelId;
    }

    public void setModelId(String modelId) {
        this.modelId = modelId == null ? null : modelId.trim();
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority == null ? null : priority.trim();
    }

    public String getReqSource() {
        return reqSource;
    }

    public void setReqSource(String reqSource) {
        this.reqSource = reqSource == null ? null : reqSource.trim();
    }

    public String getReqApplicant() {
        return reqApplicant;
    }

    public void setReqApplicant(String reqApplicant) {
        this.reqApplicant = reqApplicant == null ? null : reqApplicant.trim();
    }

    public String getReqCollector() {
        return reqCollector;
    }

    public void setReqCollector(String reqCollector) {
        this.reqCollector = reqCollector == null ? null : reqCollector.trim();
    }

    public Date getResearchDate() {
        return researchDate;
    }

    public void setResearchDate(Date researchDate) {
        this.researchDate = researchDate;
    }

    public Date getConfirmDate() {
        return confirmDate;
    }

    public void setConfirmDate(Date confirmDate) {
        this.confirmDate = confirmDate;
    }

    public String getEstimate() {
        return estimate;
    }

    public void setEstimate(String estimate) {
        this.estimate = estimate == null ? null : estimate.trim();
    }

    public String getPlanResolutionVersion() {
        return planResolutionVersion;
    }

    public void setPlanResolutionVersion(String planResolutionVersion) {
        this.planResolutionVersion = planResolutionVersion == null ? null : planResolutionVersion.trim();
    }

    public String getRealResolutionVersion() {
        return realResolutionVersion;
    }

    public void setRealResolutionVersion(String realResolutionVersion) {
        this.realResolutionVersion = realResolutionVersion == null ? null : realResolutionVersion.trim();
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment == null ? null : comment.trim();
    }

    public String getOriginalRequirement() {
        return originalRequirement;
    }

    public void setOriginalRequirement(String originalRequirement) {
        this.originalRequirement = originalRequirement == null ? null : originalRequirement.trim();
    }

    public String getUserStory() {
        return userStory;
    }

    public void setUserStory(String userStory) {
        this.userStory = userStory == null ? null : userStory.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", modelId=").append(modelId);
        sb.append(", priority=").append(priority);
        sb.append(", reqSource=").append(reqSource);
        sb.append(", reqApplicant=").append(reqApplicant);
        sb.append(", reqCollector=").append(reqCollector);
        sb.append(", researchDate=").append(researchDate);
        sb.append(", confirmDate=").append(confirmDate);
        sb.append(", estimate=").append(estimate);
        sb.append(", planResolutionVersion=").append(planResolutionVersion);
        sb.append(", realResolutionVersion=").append(realResolutionVersion);
        sb.append(", comment=").append(comment);
        sb.append(", originalRequirement=").append(originalRequirement);
        sb.append(", userStory=").append(userStory);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}