package org.nocoder.requirement.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author Created by YANGJINLONG on 2017-11-1.
 */
@Controller
public class HomeController {

    @RequestMapping("/")
    public String index(){

        return "index";
    }
}
